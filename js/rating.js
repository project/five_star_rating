(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.rating = {
    attach: function (context, settings) {
      var nid = drupalSettings.nid;
      var uid = drupalSettings.uid;
      var rating_type = drupalSettings.rating_type;
      var rating = 0;
      $('#star_rating_' + nid).barrating('set', rating);
      $('.rating').barrating({
        theme: 'fontawesome-' + rating_type,
        onSelect: function (value, text, event) {
          var el = this;
          var el_id = el.$elem.data('id');
          if (typeof (event) !== 'undefined') {
            var split_id = el_id.split("_");
            var nid = split_id[1];
            $.ajax({
              url: '/rating/set',
              type: 'post',
              data: {
                nid: nid,
                uid: uid,
                rating: value,
              },
              dataType: 'json',
              success: function (data) {
                var average = data['numRating'];
                $('#numeric_rating_' + nid).text(average);
              }
            });
          }
        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
