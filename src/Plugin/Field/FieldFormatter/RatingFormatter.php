<?php

namespace Drupal\rating\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'MyFieldFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "RatingFormatter",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "rating"
 *   }
 * )
 */
class RatingFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, FormBuilderInterface $formBuilder, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->formBuilder = $formBuilder;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('form_builder'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rating_type' => 'stars',
      'rating_size' => 'medium',
      'rating_color' => 'yellow',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['rating_type'] = [
      '#title' => $this->t('Type'),
      '#type' => 'select',
      '#options' => [
        'bombs' => $this->t('Bombs'),
        'diamonds' => $this->t('Diamonds'),
        'hearts' => $this->t('Hearts'),
        'paws' => $this->t('Paws'),
        'rockets' => $this->t('Rockets'),
        'skulls' => $this->t('Skulls'),
        'stars' => $this->t('Stars'),
        'trees' => $this->t('Trees'),
        'wrenches' => $this->t('Wrenches'),
      ],
      '#default_value' => $this->getSetting('rating_type'),
    ];

    $element['rating_size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'select',
      '#options' => [
        'small' => $this->t('Small'),
        'medium' => $this->t('Medium'),
        'large' => $this->t('Large'),
      ],
      '#default_value' => $this->getSetting('rating_size'),
    ];

    $element['rating_color'] = [
      '#title' => $this->t('Color'),
      '#type' => 'select',
      '#options' => [
        'black' => $this->t('Black'),
        'blue' => $this->t('Blue'),
        'green' => $this->t('Green'),
        'red' => $this->t('Red'),
        'yellow' => $this->t('Yellow'),
        'white' => $this->t('White'),
      ],
      '#default_value' => $this->getSetting('rating_color'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Look and feel of the rating widget.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $entity = $items->getEntity();
    $settings = $this->getSettings();
    $id = $entity->id();
    foreach ($items as $delta => $item) {
      if ($item->rating_enable == 1) {
        $options['rating_id'] = $id;
        $options['rating_size'] = $settings['rating_size'];
        $options['rating_color'] = $settings['rating_color'];
        $options['rating_type'] = $settings['rating_type'];
        $form = $this->formBuilder->getForm('Drupal\rating\Form\RatingForm', $options);
        $elements[$delta] = [
          '#markup' => render($form),
        ];
      }
    }

    return $elements;
  }

}
