<?php

namespace Drupal\rating\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'RatingWidget' widget.
 *
 * @FieldWidget(
 *   id = "RatingWidget",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "rating"
 *   }
 * )
 */
class RatingWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['rating_enable'] = [
      '#type' => 'checkbox',
      '#title' => 'Enable ratings',
      '#default_value' => $items[$delta]->rating_enable ?? NULL,
      '#weight' => 0,
    ];
    return $element;
  }

}
