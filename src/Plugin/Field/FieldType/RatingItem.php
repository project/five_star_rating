<?php

namespace Drupal\rating\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'rating' field type.
 *
 * @FieldType(
 *   id = "rating",
 *   label = @Translation("Rating"),
 *   description = @Translation("This field is used to store alpha-numeric values."),
 *   default_widget = "RatingWidget",
 *   default_formatter = "RatingFormatter"
 * )
 */
class RatingItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['rating_enable'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $definition) {
    $schema = [
      'columns' => [
        'rating_enable' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->getValue();
    if (isset($value['rating_enable']) && $value['rating_enable'] != '') {
      return FALSE;
    }
    return TRUE;
  }

}
