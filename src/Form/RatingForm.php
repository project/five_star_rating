<?php

namespace Drupal\rating\Form;

use Drupal\Core\Form\FormBase;
use Drupal\rating\Utility\Utility;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class RatingForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountInterface $account) {
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rating_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = []) {
    $uid = $this->account->id();
    $nid = $options['rating_id'];
    $star_rating = Utility::getStarRating($uid, $nid);
    $rating = Utility::getRating($nid);
    $rating_text = $rating;
    if ($rating <= 0) {
      $rating_text = 'No ratings given.';
    }
    $form['#attributes'] = [
      'class' => [$options['rating_size'], $options['rating_color']],
    ];
    $form['star_rating_option'] = [
      '#type' => 'select',
      '#options' => [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
      ],
      '#default_value' => $star_rating,
      '#attributes' => [
        'id' => 'star_rating_' . $options['rating_id'],
        'data-id' => 'rating_' . $options['rating_id'],
        'class' => ['rating'],
      ],
      '#attached' => [
        'library' => ['rating/rating-js'],
        'drupalSettings' => [
          'nid' => $nid,
          'uid' => $uid,
          'rating_type' => $options['rating_type'],
        ],
      ],
    ];
    $numeric_rating = $this->t('Rating : <span id="numeric_rating_@ratingId">@rating</span>', [
      '@ratingId' => $options['rating_id'],
      '@rating' => $rating_text,
    ]);
    $form['numeric_rating'] = [
      '#markup' => $numeric_rating,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
