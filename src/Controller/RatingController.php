<?php

namespace Drupal\rating\Controller;

use Drupal\rating\Utility\Utility;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Processes rating input.
 */
class RatingController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  private $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Sets the rating value in the stars and the displayed text.
   */
  public function set() {
    $user = $this->requestStack->getCurrentRequest()->request->get('uid');
    $node = $this->requestStack->getCurrentRequest()->request->get('nid');
    $rating = $this->requestStack->getCurrentRequest()->request->get('rating');
    Utility::setRating($user, $node, $rating);
    $numRating = Utility::getRating($node);
    $return_arr = ["numRating" => $numRating];
    return new JsonResponse($return_arr);
  }

}
