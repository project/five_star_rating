5-Star Rating


This module requires three step to install.

1. Enable the five_star_rating module.

2. Add jquery-bar-rating plugin to DRUPAL_ROOT/libraries/jquery-bar-rating.
You can download it here: https://github.com/antennaio/jquery-bar-rating

3. Add font-awesome plugin to DRUPAL_ROOT/libraries/font-awesome
You can download it here: https://github.com/FortAwesome/Font-Awesome